//
//  main.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

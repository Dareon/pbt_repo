//
//  PBTPerson+Mapping.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTPerson.h"
#import <FastEasyMapping.h>

@interface PBTPerson (Mapping)

+ (FEMMapping *)defaultMapping;

@end

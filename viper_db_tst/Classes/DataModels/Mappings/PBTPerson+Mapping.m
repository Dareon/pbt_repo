//
//  PBTPerson+Mapping.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTPerson+Mapping.h"

@implementation PBTPerson (Mapping)

+ (FEMMapping *)defaultMapping
{
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[PBTPerson entityName]];
    mapping.primaryKey = [PBTPersonAttributes name];
    
    [mapping addAttributesFromArray:@[
                                      [PBTPersonAttributes name],
                                      [PBTPersonAttributes location],
                                      [PBTPersonAttributes bio]
                                      ]];
    FEMAttribute *imageAttribute = [[FEMAttribute alloc] initWithProperty:[PBTPersonAttributes image]
                                                                  keyPath:[PBTPersonAttributes image]
                                                                      map:^id _Nullable(id  _Nullable value)
                                    {
                                        NSString *imgPath = [[NSBundle mainBundle] pathForResource:value ofType:nil];
                                        UIImage *image = [UIImage imageWithContentsOfFile:imgPath];
                                        return UIImagePNGRepresentation(image);
                                    }
                                                               reverseMap:nil];
    [mapping addAttribute:imageAttribute];
    
    return mapping;
}

@end

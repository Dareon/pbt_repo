// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PBTPerson.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface PBTPersonID : NSManagedObjectID {}
@end

@interface _PBTPerson : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) PBTPersonID *objectID;

@property (nonatomic, strong, nullable) NSString* bio;

@property (nonatomic, strong, nullable) NSData* image;

@property (nonatomic, strong, nullable) NSString* location;

@property (nonatomic, strong, nullable) NSString* name;

@end

@interface _PBTPerson (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBio;
- (void)setPrimitiveBio:(nullable NSString*)value;

- (nullable NSData*)primitiveImage;
- (void)setPrimitiveImage:(nullable NSData*)value;

- (nullable NSString*)primitiveLocation;
- (void)setPrimitiveLocation:(nullable NSString*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

@end

@interface PBTPersonAttributes: NSObject 
+ (NSString *)bio;
+ (NSString *)image;
+ (NSString *)location;
+ (NSString *)name;
@end

NS_ASSUME_NONNULL_END

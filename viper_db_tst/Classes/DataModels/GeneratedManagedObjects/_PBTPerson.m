// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PBTPerson.m instead.

#import "_PBTPerson.h"

@implementation PBTPersonID
@end

@implementation _PBTPerson

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PBTPerson" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PBTPerson";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PBTPerson" inManagedObjectContext:moc_];
}

- (PBTPersonID*)objectID {
	return (PBTPersonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic bio;

@dynamic image;

@dynamic location;

@dynamic name;

@end

@implementation PBTPersonAttributes 
+ (NSString *)bio {
	return @"bio";
}
+ (NSString *)image {
	return @"image";
}
+ (NSString *)location {
	return @"location";
}
+ (NSString *)name {
	return @"name";
}
@end


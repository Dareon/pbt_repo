//
//  PBTServiceComponents.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PBTUserDataService;

@protocol PBTServiceComponents <NSObject>

- (id <PBTUserDataService>)userDataService;

@end

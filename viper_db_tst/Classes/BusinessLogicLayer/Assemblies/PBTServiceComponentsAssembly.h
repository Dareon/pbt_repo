//
//  PBTServiceComponentsAssembly.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "PBTServiceComponents.h"

@protocol PBTCoreComponents;

@interface PBTServiceComponentsAssembly : TyphoonAssembly <RamblerInitialAssembly, PBTServiceComponents>

@property (strong, nonatomic, readonly) TyphoonAssembly <PBTCoreComponents> *coreComponents;

@end

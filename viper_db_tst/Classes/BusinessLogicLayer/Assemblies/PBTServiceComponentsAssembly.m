//
//  PBTServiceComponentsAssembly.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTServiceComponentsAssembly.h"
#import "PBTCoreComponents.h"

#import "PBTUserDataServiceImplementation.h"

@implementation PBTServiceComponentsAssembly

- (id<PBTUserDataService>)userDataService
{
    return [TyphoonDefinition withClass:[PBTUserDataServiceImplementation class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(coreDataStorageManager)
                                      with:[self.coreComponents coreDataStorageManager]];
            }];
}

@end

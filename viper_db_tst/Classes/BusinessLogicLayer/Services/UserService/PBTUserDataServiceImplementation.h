//
//  PBTUserServiceImplementation.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBTUserDataService.h"

@protocol PBTCoreDataStorageManager;

@interface PBTUserDataServiceImplementation : NSObject <PBTUserDataService>

@property (nonatomic, strong) id <PBTCoreDataStorageManager> coreDataStorageManager;

@end

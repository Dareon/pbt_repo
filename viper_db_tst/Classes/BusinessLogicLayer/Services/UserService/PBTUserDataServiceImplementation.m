//
//  PBTUserServiceImplementation.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTUserDataServiceImplementation.h"
#import "PBTCoreDataStorageManager.h"

@implementation PBTUserDataServiceImplementation

- (void)initDatabaseIfNeeded
{
    [self.coreDataStorageManager setUpDatabase];
}

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)delegate
{
    return [self.coreDataStorageManager getUserDataFRCWithDelegate:delegate];
}

- (NSString *)getBioText:(NSManagedObject *)object
{
    return [self.coreDataStorageManager getBioText:object];
}

- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object
{
    [self.coreDataStorageManager saveBioText:text forObject:object];
}

@end

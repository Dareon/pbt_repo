//
//  PBTCoreDataStorageManagerImplementation.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTCoreDataStorageManagerImplementation.h"
#import "PBTPerson+Mapping.h"

NSString *kPBTDBSetupKey = @"kPBTDBSetupKey";

@implementation PBTCoreDataStorageManagerImplementation

- (void)setUpDatabase
{
    BOOL isDbReady = [[[NSUserDefaults standardUserDefaults] objectForKey:kPBTDBSetupKey] boolValue];
    
    if(!isDbReady)
    {
        NSArray *sourceArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"source_data" ofType:@"plist"]];
        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext)
        {
            [FEMDeserializer collectionFromRepresentation:sourceArray
                                                  mapping:[PBTPerson defaultMapping]
                                                  context:localContext];
        }];

        
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:YES]
                                                  forKey:kPBTDBSetupKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"=== Core Data DB setup proceeded");
    }
    
    NSLog(@"=== All persons in DB: %@",[PBTPerson MR_findAll]);
}

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)delegate
{
    return [PBTPerson MR_fetchAllSortedBy:[PBTPersonAttributes name]
                                ascending:YES
                            withPredicate:nil
                                  groupBy:nil
                                 delegate:delegate];
}

- (NSString *)getBioText:(NSManagedObject *)object
{
    if([object isKindOfClass:[PBTPerson class]])
    {
        return ((PBTPerson *)object).bio;
    }
    else
    {
        return @"Smth went wrong!";
    }
}

- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object
{
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext * _Nonnull localContext)
    {
        if([object isKindOfClass:[PBTPerson class]])
        {
            PBTPerson *person = [((PBTPerson *)object) MR_inContext:localContext];
            person.bio = text;
        }
        else
        {
            NSLog(@"current bio object is wrong!");
        }
    }];
    
}

@end

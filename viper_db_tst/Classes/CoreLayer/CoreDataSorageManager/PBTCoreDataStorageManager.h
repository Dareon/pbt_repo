//
//  PBTCoreDataStorageManager.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PBTPerson;

@protocol PBTCoreDataStorageManager <NSObject>

- (void)setUpDatabase;
- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)delegate;
- (NSString *)getBioText:(NSManagedObject *)object;
- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object;

@end

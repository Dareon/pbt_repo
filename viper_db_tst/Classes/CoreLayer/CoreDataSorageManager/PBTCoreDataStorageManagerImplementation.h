//
//  PBTCoreDataStorageManagerImplementation.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PBTCoreDataStorageManager.h"

@interface PBTCoreDataStorageManagerImplementation : NSObject <PBTCoreDataStorageManager>

@end

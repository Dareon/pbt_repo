//
//  PBTCoreComponentsAssembly.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>
#import "PBTCoreComponents.h"

@interface PBTCoreComponentsAssembly : TyphoonAssembly <RamblerInitialAssembly, PBTCoreComponents>

@end

//
//  PBTCoreComponentsAssembly.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTCoreComponentsAssembly.h"
#import "PBTCoreDataStorageManagerImplementation.h"

@implementation PBTCoreComponentsAssembly

- (id<PBTCoreDataStorageManager>)coreDataStorageManager
{
    return [TyphoonDefinition withClass:[PBTCoreDataStorageManagerImplementation class]];
}

@end

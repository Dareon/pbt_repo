//
//  PBTCollectionViewCell.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 12.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTCollectionViewCell.h"
#import "PBTPerson.h"

@interface PBTCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *positionLabel;

@end

@implementation PBTCollectionViewCell

- (void)updateCellWithObject:(NSManagedObject *)object
{
    if([object isMemberOfClass:[PBTPerson class]])
    {
        PBTPerson *person = (PBTPerson *)object;
        
        self.avatarImageView.image      = [UIImage imageWithData:person.image];
        
        self.nameLabel.text             = person.name;
        self.positionLabel.text         = person.location;
    }
    else
    {
        NSLog(@"object is not a %@ class member", NSStringFromClass([PBTPerson class]));
    }
}

@end

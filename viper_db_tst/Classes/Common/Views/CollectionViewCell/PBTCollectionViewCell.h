//
//  PBTCollectionViewCell.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 12.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kPBTCollectionViewLeftAlignedCellIdentifier = @"leftAlignedCell";
static NSString *kPBTCollectionViewRightAlignedCellIdentifier = @"rightAlignedCell";

@interface PBTCollectionViewCell : UICollectionViewCell

- (void)updateCellWithObject:(NSManagedObject *)object;


@end

//
//  PBTBlockOperation.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTBlockOperation.h"

@implementation PBTBlockOperation

- (BOOL)isAsynchronous
{
    return NO;
}

- (void)start
{
    for (void (^executionBlock)(void) in [self executionBlocks])
    {
        executionBlock();
    }
}

@end

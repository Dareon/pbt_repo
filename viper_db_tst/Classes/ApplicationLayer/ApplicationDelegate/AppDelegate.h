//
//  AppDelegate.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PBTApplicationConfigurator;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) id <PBTApplicationConfigurator> applicationConfigurator;
@property (strong, nonatomic) UIWindow *window;

@end


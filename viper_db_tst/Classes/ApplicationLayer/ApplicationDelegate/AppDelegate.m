//
//  AppDelegate.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "AppDelegate.h"
#import "PBTApplicationConfigurator.h"
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.applicationConfigurator configureAppDelegate:self];

    return YES;
}

- (NSArray *)initialAssemblies
{
    RamblerInitialAssemblyCollector *collector = [RamblerInitialAssemblyCollector new];
    return [collector collectInitialAssemblyClasses];
}

@end

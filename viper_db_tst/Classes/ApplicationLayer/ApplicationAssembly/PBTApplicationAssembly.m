//
//  PBTApplicationAssembly.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTApplicationAssembly.h"

#import "AppDelegate.h"
#import "PBTApplicationConfiguratorImplementation.h"

@implementation PBTApplicationAssembly

- (AppDelegate *)appDelegate
{
    return [TyphoonDefinition withClass:[AppDelegate class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(applicationConfigurator)
                                      with:[self applicationConfigurator]];
            }];
}

- (id <PBTApplicationConfigurator>)applicationConfigurator
{
    return [TyphoonDefinition withClass:[PBTApplicationConfiguratorImplementation class]];
}

@end

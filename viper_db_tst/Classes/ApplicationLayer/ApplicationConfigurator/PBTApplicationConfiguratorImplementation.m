//
//  PBTApplicationConfiguratorImplementation.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTApplicationConfiguratorImplementation.h"
#import "PBTApplicationConfigurator.h"

#import <MagicalRecord/MagicalRecord.h>

static NSString * const kCoreDataStoreName = @"viper_db_tst";

@implementation PBTApplicationConfiguratorImplementation

- (void)configureAppDelegate:(id<UIApplicationDelegate>)appDelegate
{
    [self setupCoreDataStack];
    [self setupWindowForAppDelegate:appDelegate];
    [self setupInitialConfiguration];
}



#pragma mark - Additional methods

- (void)setupInitialConfiguration
{
    
}

- (void)setupWindowForAppDelegate:(id<UIApplicationDelegate>)appDelegate
{
    appDelegate.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    appDelegate.window.backgroundColor = [UIColor whiteColor];
    appDelegate.window.rootViewController = [[UIStoryboard mainStoryboard] instantiateInitialViewController];
    
    [[UINavigationBar appearance] setBarStyle:UIBarStyleBlack];
    [[UINavigationBar appearance] setTintColor:[UIColor mainTintColor]];
    appDelegate.window.tintColor = [UIColor mainTintColor];
    
    [appDelegate.window makeKeyAndVisible];
}

- (void)setupCoreDataStack
{
    [MagicalRecord setupCoreDataStackWithStoreNamed:kCoreDataStoreName];
}


@end

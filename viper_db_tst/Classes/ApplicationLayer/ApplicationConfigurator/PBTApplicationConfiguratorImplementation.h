//
//  PBTApplicationConfiguratorImplementation.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBTApplicationConfigurator.h"

@protocol PBTApplicationConfigurator;

@interface PBTApplicationConfiguratorImplementation : NSObject <PBTApplicationConfigurator>

@end

//
//  PBTApplicationConfigurator.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UIApplicationDelegate;

@protocol PBTApplicationConfigurator <NSObject>

- (void)configureAppDelegate:(id <UIApplicationDelegate>)appDelegate;

@end

//
//  PBTMainScreenRouter.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenRouterInput.h"

@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface PBTMainScreenRouter : NSObject <PBTMainScreenRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end

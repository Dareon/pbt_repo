//
//  PBTMainScreenAssembly.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenAssembly.h"

#import "PBTMainScreenViewController.h"
#import "PBTBottomSheetViewController.h"
#import "PBTMainScreenInteractor.h"
#import "PBTMainScreenPresenter.h"
#import "PBTMainScreenRouter.h"

#import "PBTServiceComponents.h"

#import <ViperMcFlurry/ViperMcFlurry.h>

@implementation PBTMainScreenAssembly

- (PBTMainScreenViewController *)viewMainScreen
{
    return [TyphoonDefinition withClass:[PBTMainScreenViewController class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(output)
                                      with:[self presenterMainScreen]];
                [definition injectProperty:@selector(moduleInput)
                                      with:[self presenterMainScreen]];
            }];
}

- (id)mainStoryboard
{
    return [TyphoonDefinition withClass:[UIStoryboard class]
                          configuration:^(TyphoonDefinition* definition)
    {
                
                [definition useInitializer:@selector(mainStoryboard)];
    }];
}

- (PBTBottomSheetViewController *)viewBottomSheet
{
    return [TyphoonDefinition withFactory:[self mainStoryboard]
                                 selector:@selector(instantiateViewControllerWithIdentifier:)
                               parameters:^(TyphoonMethod *factoryMethod)
    {
        [factoryMethod injectParameterWith:@"PBTBottomSheetViewController"];
    }
                            configuration:^(TyphoonFactoryDefinition *definition)
    {
        [definition injectProperty:@selector(output)
                              with:[self presenterMainScreen]];
    }];
//    return [TyphoonDefinition withClass:[PBTBottomSheetViewController class]
//                          configuration:^(TyphoonDefinition *definition)
//            {
//                [definition useInitializer:@selector(s) parameters:]
//                [definition injectProperty:@selector(output)
//                                      with:[self presenterMainScreen]];
//            }];
}

- (PBTMainScreenInteractor *)interactorMainScreen
{
    return [TyphoonDefinition withClass:[PBTMainScreenInteractor class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(output)
                                      with:[self presenterMainScreen]];
                [definition injectProperty:@selector(userDataService)
                                      with:[self.serviceComponents userDataService]];
            }];
}

- (PBTMainScreenPresenter *)presenterMainScreen
{
    return [TyphoonDefinition withClass:[PBTMainScreenPresenter class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(view)
                                      with:[self viewMainScreen]];
                [definition injectProperty:@selector(bottomSheetView)
                                      with:[self viewBottomSheet]];
                [definition injectProperty:@selector(interactor)
                                      with:[self interactorMainScreen]];
                [definition injectProperty:@selector(router)
                                      with:[self routerMainScreen]];
            }];
}

- (PBTMainScreenRouter *)routerMainScreen
{
    return [TyphoonDefinition withClass:[PBTMainScreenRouter class]
                          configuration:^(TyphoonDefinition *definition)
            {
                [definition injectProperty:@selector(transitionHandler)
                                      with:[self viewMainScreen]];
            }];
}

@end

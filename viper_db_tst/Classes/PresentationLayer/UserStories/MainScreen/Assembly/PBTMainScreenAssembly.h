//
//  PBTMainScreenAssembly.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

@protocol PBTServiceComponents;

@interface PBTMainScreenAssembly : TyphoonAssembly <RamblerInitialAssembly>

@property (nonatomic, strong) TyphoonAssembly <PBTServiceComponents> *serviceComponents;

@end

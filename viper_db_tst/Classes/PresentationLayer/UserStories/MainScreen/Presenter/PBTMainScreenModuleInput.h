//
//  PBTMainScreenModuleInput.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ViperMcFlurry/ViperMcFlurry.h>

@protocol PBTMainScreenModuleInput <RamblerViperModuleInput>

- (void)configureModule;

@end

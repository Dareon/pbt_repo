//
//  PBTMainScreenPresenter.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenViewOutput.h"
#import "PBTMainScreenInteractorOutput.h"
#import "PBTMainScreenModuleInput.h"
#import "PBTBottomSheetViewOutput.h"

@protocol PBTMainScreenViewInput;
@protocol PBTMainScreenInteractorInput;
@protocol PBTMainScreenRouterInput;
@protocol PBTBottomSheetViewInput;

@interface PBTMainScreenPresenter : NSObject <PBTMainScreenModuleInput, PBTMainScreenViewOutput, PBTMainScreenInteractorOutput, PBTBottomSheetViewOutput>

@property (nonatomic, weak) id<PBTMainScreenViewInput> view;
@property (nonatomic, strong) id<PBTBottomSheetViewInput> bottomSheetView;
@property (nonatomic, strong) id<PBTMainScreenInteractorInput> interactor;
@property (nonatomic, strong) id<PBTMainScreenRouterInput> router;

@end

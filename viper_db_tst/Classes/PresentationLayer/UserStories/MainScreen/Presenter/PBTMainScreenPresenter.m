//
//  PBTMainScreenPresenter.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenPresenter.h"

#import "PBTMainScreenViewInput.h"
#import "PBTMainScreenInteractorInput.h"
#import "PBTMainScreenRouterInput.h"
#import "PBTBottomSheetViewInput.h"

@implementation PBTMainScreenPresenter

#pragma mark - Методы PBTMainScreenModuleInput

- (void)configureModule
{

}

#pragma mark - Методы PBTMainScreenViewOutput

- (void)didTriggerViewReadyEvent
{
    [self.interactor didTriggerViewReadyEvent];
    
    [self.view setupInitialState];
}

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)deledate
{
    return [self.interactor getUserDataFRCWithDelegate:deledate];
}

- (void)didTapCollectionViewCell:(NSManagedObject *)object
{
    [self.view displayBottomSheet:self.bottomSheetView];
    NSString *bioText = [self.interactor getBioText:object];
    [self.bottomSheetView setBioText:bioText activeObject:object];
}

#pragma mark - PBTBottomSheetViewOutput

- (void)didTriggerBottomViewReadyEvent
{
    [self.bottomSheetView setupInitialState];
}

- (void)didTapCloseBottomSheetButton
{
    [self.view hideBottomSheet];
}

- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object
{
    [self.interactor saveBioText:text forObject:object];
}

#pragma mark - Методы PBTMainScreenInteractorOutput

@end

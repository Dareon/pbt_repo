//
//  PBTMainScreenInteractorInput.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PBTMainScreenInteractorInput <NSObject>

- (void)didTriggerViewReadyEvent;

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)deledate;
- (NSString *)getBioText:(NSManagedObject *)object;

- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object;

@end

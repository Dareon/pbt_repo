//
//  PBTMainScreenInteractor.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenInteractorInput.h"

@protocol PBTMainScreenInteractorOutput;
@protocol PBTUserDataService;

@interface PBTMainScreenInteractor : NSObject <PBTMainScreenInteractorInput>

@property (nonatomic, weak)     id<PBTMainScreenInteractorOutput>   output;

@property (nonatomic, strong)   id<PBTUserDataService>              userDataService;

@end

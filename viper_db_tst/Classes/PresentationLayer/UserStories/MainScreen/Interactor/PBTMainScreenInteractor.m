//
//  PBTMainScreenInteractor.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenInteractor.h"

#import "PBTMainScreenInteractorOutput.h"
#import "PBTUserDataService.h"

@implementation PBTMainScreenInteractor

#pragma mark - Методы PBTMainScreenInteractorInput

- (void)didTriggerViewReadyEvent
{
    [self.userDataService initDatabaseIfNeeded];
}

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)deledate
{
    return [self.userDataService getUserDataFRCWithDelegate:deledate];
}

- (NSString *)getBioText:(NSManagedObject *)object
{
    return [self.userDataService getBioText:object];
}

- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object
{
    [self.userDataService saveBioText:text forObject:object];
}

@end

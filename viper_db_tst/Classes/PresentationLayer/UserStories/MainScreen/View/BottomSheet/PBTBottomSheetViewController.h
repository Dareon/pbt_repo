//
//  PBTBottomSheetViewController.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBTBottomSheetViewInput.h"

static NSString *PBTBottomSheetViewControllerStoryboardIdentifier = @"PBTBottomSheetViewController";

@protocol PBTBottomSheetViewOutput;

@interface PBTBottomSheetViewController : UIViewController <PBTBottomSheetViewInput>

@property (nonatomic, strong) id<PBTBottomSheetViewOutput> output;
@property (weak, nonatomic) IBOutlet UIView *topPanelView;

@end

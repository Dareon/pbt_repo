//
//  PBTBottomSheetViewController.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTBottomSheetViewController.h"
#import "PBTBottomSheetViewOutput.h"
#import <PureLayout.h>

@interface PBTBottomSheetViewController ()

@property (weak, nonatomic) IBOutlet UITextView *mainTextView;
@property (strong, nonatomic) NSManagedObject *activeObject;

@end

@implementation PBTBottomSheetViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.output didTriggerBottomViewReadyEvent];

}

- (BOOL)automaticallyAdjustsScrollViewInsets
{
    return NO;
}



- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - PBTBottomSheetViewInput

- (void)setupInitialState
{
    self.view.layer.cornerRadius = 5.;
    self.view.layer.borderColor = [UIColor mainTintColor].CGColor;
    self.view.layer.borderWidth = 1.;
    
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualEffect = [[UIVisualEffectView alloc] initWithEffect:blur];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blur];
    [visualEffectView.contentView addSubview:visualEffect];
    
    [self.view insertSubview:visualEffectView atIndex:0];
    [visualEffectView autoPinEdgesToSuperviewEdges];
}

- (void)setBioText:(NSString *)text activeObject:(NSManagedObject *)object
{
    self.mainTextView.text = text;
    self.activeObject = object;
}

#pragma mark - Actions

- (IBAction)closeSheetButtonTapped:(UIButton *)sender
{
    [self.output didTapCloseBottomSheetButton];
    [self.mainTextView resignFirstResponder];
}

- (IBAction)saveChangesButtonTapped:(UIButton *)sender
{
    [self.output saveBioText:self.mainTextView.text forObject:self.activeObject];
    [self.mainTextView resignFirstResponder];
}


@end

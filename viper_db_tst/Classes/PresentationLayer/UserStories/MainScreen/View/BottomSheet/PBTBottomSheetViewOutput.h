//
//  PBTBottomSheetViewOutput.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 13.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PBTBottomSheetViewOutput <NSObject>

- (void)didTriggerBottomViewReadyEvent;
- (void)didTapCloseBottomSheetButton;
- (void)saveBioText:(NSString *)text forObject:(NSManagedObject *)object;

@end

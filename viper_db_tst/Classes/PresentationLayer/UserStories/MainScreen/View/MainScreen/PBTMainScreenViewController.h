//
//  PBTMainScreenViewController.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PBTMainScreenViewInput.h"

@protocol PBTMainScreenViewOutput;

@interface PBTMainScreenViewController : UIViewController <PBTMainScreenViewInput>

@property (nonatomic, strong) id<PBTMainScreenViewOutput> output;

@end

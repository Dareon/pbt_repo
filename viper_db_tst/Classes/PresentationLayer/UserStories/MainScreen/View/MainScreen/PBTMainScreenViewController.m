//
//  PBTMainScreenViewController.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "PBTMainScreenViewController.h"
#import "PBTBottomSheetViewController.h"

#import "PBTMainScreenViewOutput.h"
#import "PBTCollectionViewCell.h"

#import <PureLayout.h>

@interface PBTMainScreenViewController () <NSFetchedResultsControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSFetchedResultsController *userDataFRC;
@property (nonatomic, strong) PBTBlockOperation *blockOperation;

@property (nonatomic, weak) PBTBottomSheetViewController *bottomSheetViewController;
@property (nonatomic, strong) NSLayoutConstraint *bottomSheetHeightConstraint;

@property (nonatomic, assign) CGFloat previousHeightChange;

@end

@implementation PBTMainScreenViewController

#pragma mark - Методы жизненного цикла

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self.output didTriggerViewReadyEvent];
}

#pragma mark - Методы PBTMainScreenViewInput

- (void)setupInitialState
{
    self.userDataFRC = [self.output getUserDataFRCWithDelegate:self];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
}

- (void)displayBottomSheet:(id)bottomSheetVC
{
    if(!self.bottomSheetViewController)
    {
        self.bottomSheetViewController = bottomSheetVC;
        [self addChildViewController:self.bottomSheetViewController];
        [self.view addSubview:self.bottomSheetViewController.view];
        [self.bottomSheetViewController didMoveToParentViewController:self];
        
        [self.bottomSheetViewController.view autoPinEdgeToSuperviewEdge:ALEdgeLeft];
        [self.bottomSheetViewController.view autoPinEdgeToSuperviewEdge:ALEdgeBottom];
        [self.bottomSheetViewController.view autoPinEdgeToSuperviewEdge:ALEdgeRight];
        self.bottomSheetHeightConstraint = [self.bottomSheetViewController.view autoSetDimension:ALDimensionHeight toSize:150.];
        //    self.bottomSheetHeightConstraint = [self.bottomSheetViewController.view autoMatchDimension:ALDimensionHeight
        //                                                                                   toDimension:ALDimensionHeight
        //                                                                                        ofView:self.view
        //                                                                                withMultiplier:0.3];

        
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self
                                                                                     action:@selector(handlePan:)];
        panGesture.delegate = self;
        [self.bottomSheetViewController.topPanelView addGestureRecognizer:panGesture];
    }
    
    self.bottomSheetViewController.view.hidden = NO;
}

- (void)hideBottomSheet
{
    self.bottomSheetViewController.view.hidden = YES;
}

#pragma mark - Additional methods

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    
//    NSLog(@"%@",NSStringFromCGPoint([recognizer translationInView:self.view]));
    
    CGPoint translation = [recognizer translationInView:self.view];
    
    CGFloat translationDelta = (translation.y - self.previousHeightChange);
    
    NSLog(@"%f",translationDelta);
    
    self.bottomSheetHeightConstraint.constant -= translationDelta;
    
    self.previousHeightChange = translation.y;
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    self.previousHeightChange = 0.;
    return YES;
}

/**
 Не выносил дата сорс в отдельную сущьность для экономии времени.
 */

#pragma mark - NSFetchedResultsControllerDelegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    self.blockOperation = [[PBTBlockOperation alloc] init];
}

- (void)controller:(NSFetchedResultsController *)controller
   didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath
     forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    
    __weak UICollectionView *collectionView = self.collectionView;
    
    switch (type)
    {
        case NSFetchedResultsChangeInsert:
        {
            [self.blockOperation addExecutionBlock:^
            {
                [collectionView insertSections:[NSIndexSet indexSetWithIndex:newIndexPath.section]];
            }];
            break;
        }
            
        case NSFetchedResultsChangeDelete:
        {
            [self.blockOperation addExecutionBlock:^
            {
                [collectionView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
            }];
            break;
        }
            
        case NSFetchedResultsChangeUpdate:
        {
            [self.blockOperation addExecutionBlock:^
            {
                [collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
            }];
            break;
        }
            
        case NSFetchedResultsChangeMove:
        {
            [self.blockOperation addExecutionBlock:^
            {
                [collectionView moveSection:indexPath.section toSection:newIndexPath.section];
            }];
            break;
        }
            
        default:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.collectionView performBatchUpdates:^
    {
        [self.blockOperation start];
    }
                                  completion:nil];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    PBTCollectionViewCell *cell = (PBTCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    
    [UIView animateWithDuration:0.15
                          delay:0.
                        options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                     animations:^
     {
         cell.transform = CGAffineTransformMakeScale(0.93, 0.93);
     }
                     completion:^(BOOL finished)
     {
         [UIView animateWithDuration:0.15
                               delay:0.
                             options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState
                          animations:^
          {
              cell.transform = CGAffineTransformIdentity;
          }
                          completion:nil];
         
     }];
    
    [self.output didTapCollectionViewCell:[self.userDataFRC objectAtIndexPath:indexPath]];
    
}



#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.userDataFRC.sections[section] numberOfObjects];
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PBTCollectionViewCell *cell = nil;
    
    if(indexPath.row%2)
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPBTCollectionViewLeftAlignedCellIdentifier
                                                         forIndexPath:indexPath];
    }
    else
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:kPBTCollectionViewRightAlignedCellIdentifier
                                                         forIndexPath:indexPath];
    }
    
    NSManagedObject *object = [self.userDataFRC objectAtIndexPath:indexPath];
    [cell updateCellWithObject:object];
    
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.userDataFRC.sections count];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.collectionView.bounds), 80.);
}

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator
{
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext>  _Nonnull context)
     {
         [self.collectionView performBatchUpdates:nil completion:nil];
     }
                                 completion:nil];
}

@end

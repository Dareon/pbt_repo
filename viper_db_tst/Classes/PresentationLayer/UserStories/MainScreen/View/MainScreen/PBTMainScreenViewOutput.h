//
//  PBTMainScreenViewOutput.h
//  viper_db_tst
//
//  Created by Mikle Manilo on 06/03/2017.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PBTMainScreenViewOutput <NSObject>

- (void)didTriggerViewReadyEvent;

- (NSFetchedResultsController *)getUserDataFRCWithDelegate:(id)deledate;

- (void)didTapCollectionViewCell:(NSManagedObject *)object;

@end

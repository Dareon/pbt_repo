//
//  UIStoryboard+PBTStories.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "UIStoryboard+PBTStories.h"

NSString *const PBTMainStoryboardName           = @"MainScreen";

@implementation UIStoryboard (PBTStories)

+ (UIStoryboard *)mainStoryboard
{
    return [UIStoryboard storyboardWithName:PBTMainStoryboardName
                                     bundle:nil];
}

@end

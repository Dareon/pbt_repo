//
//  UIColor+PBTAppTheme.m
//  viper_db_tst
//
//  Created by Mikle Manilo on 06.03.17.
//  Copyright © 2017 Mikle Manilo. All rights reserved.
//

#import "UIColor+PBTAppTheme.h"

@implementation UIColor (PBTAppTheme)

+ (UIColor *)mainTintColor
{
    return [UIColor colorWithRed:10./255. green:150./255. blue:196./255. alpha:1.];
}

@end
